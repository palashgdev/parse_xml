import express from "express";
import { gets, saves, getAlls } from "./controller"

const route = express.Router();

route.post("/save", saves);
route.post("/get", gets);
route.post("/", getAlls);

export default route;