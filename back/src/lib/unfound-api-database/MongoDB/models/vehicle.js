import mongoose, { Schema } from "mongoose";

const DataSchema = new Schema({
  id: String,
  frame: String,
  wheels: String,
  powertrain: String
}, { timestamps: true });


export default mongoose.model("Ek_vehicle", DataSchema);