import express from "express";
import morgan from "morgan"
import helmet from "helmet";
import compression from "compression";

export default (app) => {
  app.use(express.json());
  app.use(helmet());

  //setting the cors
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  app.use(compression());
  app.use(morgan("dev"));
}