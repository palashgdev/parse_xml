import axios from "axios";


export async function upload({ file }) {
  try {
    const url = `http://localhost:3000/api/upload`;

    const formdata = new FormData();
    formdata.append("selectedFile", file);

    const res = await axios.post(url, formdata, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    });

    const { statusCode } = res;

    if (statusCode >= 300) {
      return {
        statusCode: statusCode,
        error: "Error"
      }
    }

    return {
      res
    }

  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: "Something went wrong"
    }
  }
}