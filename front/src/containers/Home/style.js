const style = (theme) => ({
  card: {
    maxWidth: 345,
    marginTop: "10%",
    marginLeft: "37%"
  },
  button: {
    marginTop: "20px",
  },
  submit: {
    marginLeft: "40px",
    marginTop: "20px",
  },
  analyse: {
    marginTop: "10px",
    marginLeft: "37%"
  },
  analyse1: {
    marginTop: "10px",
    marginLeft: "45%"
  },
  root: {
    marginTop: "60px",
    marginLeft: "37%",
    maxWidth: 345,
  },
  close: {
    padding: theme.spacing.unit / 2,
  },
});

export default style;