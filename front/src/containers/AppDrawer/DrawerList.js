import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from '@material-ui/core/Divider';
import CardMedia from "@material-ui/core/CardMedia"

import style from './DrawerList.style';

class DrawerList extends Component {
  onClick = (stage) => () => {
    window.location.href = stage;
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div className={classes.list}>
          <CardMedia
            className={classes.media}
            image="https://pbs.twimg.com/profile_images/956905429593833472/TyV6xtVa_400x400.jpg"
            title="logo"
          />
          <Divider />
          <List component="nav">
            <ListItem button onClick={this.onClick("/")}>
              <ListItemText primary="Home" />
            </ListItem>
            <Divider />
            <ListItem button onClick={this.onClick("/reports")}>
              <ListItemText primary="All Reports" />
            </ListItem>
          </List>
        </div>
      </div>
    );
  }
}

export default withStyles(style)(DrawerList)